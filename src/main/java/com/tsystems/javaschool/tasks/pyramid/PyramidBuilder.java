package com.tsystems.javaschool.tasks.pyramid;
import java.util.*;

public class PyramidBuilder {

    /**
     * Builds a pyramid with sorted values (with minumum value at the top line and maximum at the bottom,
     * from left to right). All vacant positions in the array are zeros.
     *
     * @param inputNumbers to be used in the pyramid
     * @return 2d array with pyramid inside
     * @throws {@link CannotBuildPyramidException} if the pyramid cannot be build with given input
     */
    public int[][] buildPyramid(List<Integer> inputNumbers) throws CannotBuildPyramidException {
        // TODO : Implement your solution here
        if (inputNumbers.contains(null))
            throw new CannotBuildPyramidException();

        int countlevel = countLevel(inputNumbers);
        if ( countlevel <= 0 ) throw new CannotBuildPyramidException();

            inputNumbers.sort((x,y)-> x - y);
        return bild(inputNumbers, countlevel);
    }

    public int countLevel(List<Integer> inputNumbers){
        int countlevel = 0;
        int N = inputNumbers.size();
        int result = 0;
        for(int i = 1; i <= inputNumbers.size(); ++i){
            if (N < 0) {
                result = -1;
                break;
            }
            else if (N == 0) {
                result = countlevel;
                break;
            }
            else
            {
                N -= i;
                countlevel++;
            }
        }
        return  result;
    }

    public int[][] bild(List<Integer> inputNumbers, int countlevel){
        int length = 2*countlevel - 1;
        int[][] outputPyramid = new int[countlevel][length];
        int index = 0;
        for (int i = 0; i < countlevel; i++) {
           for(int j = length / 2 - i; j <= length/2 + i; j += 2)
               outputPyramid[i][j] = inputNumbers.get(index++);
        }
        return outputPyramid;
    }


}
