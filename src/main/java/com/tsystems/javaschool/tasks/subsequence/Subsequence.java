package com.tsystems.javaschool.tasks.subsequence;

import java.util.List;

public class Subsequence {

    /**
     * Checks if it is possible to get a sequence which is equal to the first
     * one by removing some elements from the second one.
     *
     * @param x first sequence
     * @param y second sequence
     * @return <code>true</code> if possible, otherwise <code>false</code>
     */
    @SuppressWarnings("rawtypes")
    public boolean find(List x, List y) {
        // TODO: Implement the logic here
         boolean result = true;
         if (x == null || y == null )
            throw new IllegalArgumentException();

         return x.isEmpty() || findsubsequence(x,y);

}

    public boolean findsubsequence(List x, List y){
        int index = 0;
        for (int i = 0; i < y.size(); i++) {

            if (index == x.size())
                return true;
            
            if(x.get(index).equals(y.get(i)))
                index++;
        }
        return false;
    }
}

