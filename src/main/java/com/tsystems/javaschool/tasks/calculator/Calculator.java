package com.tsystems.javaschool.tasks.calculator;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.Stack;

public class Calculator {

    public String evaluate(String statement) {
        // TODO: Implement the logic here
        if (statement != null && statement != "" && CheckCorrectExpression(ConvertExpression(statement))){
            Double result = Counting(ConvertToPostfix(statement));
            if (result != null){
                double d = round(result, 4);
                if ((int)d == d) {
                    return String.valueOf((int)d);
                }
                return String.valueOf(d);
            }
        }
        return null;
    }
    public boolean isSpace(char c){
        if (c == ' ') return true;
        return false;
    }

    public boolean isOperator(char c){
        if ("+-*/()".indexOf(c) != -1) return true;
        return  false;
    }

    public int GetPrioritet(char c){
        switch (c)
        {
            case '(':
                return 0;
            case ')':
                return 0;
            case '*':
                return 2;
            case '/':
                return 2;
            case '+':
                return 1;
            case '-':
                return 1;
            default:
                return 3;
        }
    }

    public String ConvertExpression(String input){
        input = input.replaceAll(" ","");
        StringBuffer stringBuffer = new StringBuffer(input);
        for (int i = 0; i < stringBuffer.length(); i++) {
            boolean flag = stringBuffer.charAt(i) == '-';
            if (flag && ((i == 0) || (stringBuffer.charAt(i-1) == '('))) stringBuffer = stringBuffer.insert(i, '0');
        }
        return stringBuffer.toString();
    }

    public boolean CheckCorrectExpression(String input){

        boolean result = true;
        Stack<Character> stack = new Stack<Character>();
        input = '(' + input + ')';
        for (int i = 0; i < input.length(); ++i)
        {
            if (isSpace(input.charAt(i)))
                continue;
            else if (input.charAt(i) == '(')
            {
                if (stack.empty() || isOperator(stack.peek()) || stack.peek() == '(')
                {
                    stack.push('(');
                }
                else
                {
                    result = false;
                    break;
                }
            }
            else if (input.charAt(i) == ')')
            {
                char c = stack.pop();
                while (c != '(')
                {
                    if (stack.empty())
                    {
                        return false;
                    }
                    c = stack.pop();
                }
                if (!result)
                    break;

                stack.push('1');
            }
            else if (isOperator(input.charAt(i)))
            {
                if (!stack.empty() && (Character.isDigit(stack.peek()) || stack.peek() == ')'))
                {
                    stack.push(input.charAt(i));
                }
                else
                {
                    result = false;
                    break;
                }
            }
            else if (Character.isDigit(input.charAt(i)))
            {
                boolean b = false;
                while (i < input.length()&& (Character.isDigit(input.charAt(i)) || input.charAt(i) == '.'))
                {
                    if (input.charAt(i) == '.'){
                        if (!b) b = true;
                        else return false;
                    }

                    i++;
                }
                --i;
                if (isOperator(stack.peek()) || stack.peek() == '(')
                {
                    stack.push('1');
                }
                else
                {
                    result = false;
                    break;
                }
            }

        }
        stack.pop();

        return result && stack.empty();
    }

    public String ConvertToPostfix(String input){
        String output = "";
        input += "+0";
        //Для операторов
        Stack<Character> stack = new Stack<Character>();

        for (int i = 0; i < input.length(); ++i)
        {
            //Если пробельный символ,то переходим к следующему
            if (isSpace(input.charAt(i)))
                continue;
            //Если символ число
            if (Character.isDigit(input.charAt(i)))
            {
                while (i < input.length() && (Character.isDigit(input.charAt(i)) || input.charAt(i) == '.'))
                {
                    output += input.charAt(i++);
                }
                output += " ";
                i--;
            }

            if (i < input.length() && isOperator(input.charAt(i)))
            {
                if (input.charAt(i) == '(')
                    stack.push('(');
                else if (input.charAt(i) == ')')
                {
                    char c = stack.pop();
                    while (c != '(')
                    {
                        output += c + " ";
                        c = stack.pop();
                    }
                }
                else
                {
                    if (!stack.empty())
                    {
                        if (GetPrioritet(input.charAt(i)) <= GetPrioritet(stack.peek()))
                        {
                            output += stack.pop().toString() + " ";
                        }
                    }
                    stack.push(input.charAt(i));
                }
            }

        }
        while (!stack.empty())
            output += stack.pop().toString() + " ";
        return output;
    }

    public Double Counting(String input){
        double result = 0;
        Stack<Double> stack = new Stack<Double>();

        for (int i = 0; i < input.length(); ++i)
        {
            if (Character.isDigit(input.charAt(i)))
            {
                String s = "";
                while (i < input.length() && !isOperator(input.charAt(i)) && !isSpace(input.charAt(i)))
                {
                    s += input.charAt(i++);
                }
                stack.push(Double.parseDouble(s));
                //--i;
            }
            else if (isOperator(input.charAt(i)))
            {
                double b = stack.pop();
                double a = stack.pop();

                switch (input.charAt(i))
                {
                    case '+':
                        result = a + b;
                        break;
                    case '-':
                        result = a - b;
                        break;
                    case '*':
                        result = a * b;
                        break;
                    case '/':
                        if (b == 0) {
                            return null;
                        }
                        result = a / b;
                        break;
                }

                stack.push(result);
            }
        }

        return result;
    }

    private Double round(Double value, int places) {
        BigDecimal bd = new BigDecimal(Double.toString(value));
        bd = bd.setScale(places, RoundingMode.HALF_UP);
        return bd.doubleValue();
    }
}
